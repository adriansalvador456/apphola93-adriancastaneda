package com.example.apphola93;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import com.example.apphola93.databinding.ActivityIngresaCotizacionBinding;

public class menuActivity extends AppCompatActivity{
    private CardView crvHola , crvImc, crvGrados, crvMoneda, crvCotizacion, crvSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_menu);
        iniciarComponentes();
        crvHola.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(menuActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
        crvImc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(menuActivity.this, IMCActivity.class);
                startActivity(intent);
            }
        });
        crvGrados.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(menuActivity.this, ConversionGrados.class);
                startActivity(intent);
            }
        });
        crvMoneda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(menuActivity.this, CambioMonedaActivity.class);
                startActivity(intent);
            }
        });
        crvCotizacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(menuActivity.this, IngresaCotizacionActivity.class);
                startActivity(intent);
            }
        });
        crvSpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(menuActivity.this, SpinnerActivity.class);
                startActivity(intent);
            }
        });
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }

    public void iniciarComponentes() {
        crvHola = (CardView) findViewById(R.id.crvHola);
        crvImc = (CardView) findViewById(R.id.crvImc);
        crvGrados = (CardView) findViewById(R.id.crvConversion);
        crvMoneda = (CardView) findViewById(R.id.crvCambio);
        crvCotizacion = (CardView) findViewById(R.id.crvCotizacion);
        crvSpinner = (CardView) findViewById(R.id.crvSpinner);
    }
}