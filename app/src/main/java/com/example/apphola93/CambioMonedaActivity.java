package com.example.apphola93;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class CambioMonedaActivity extends AppCompatActivity {

    private EditText txtCantidad;
    private TextView txtResultado;
    private Spinner spinner;
    private Button btnCalcular, btnLimpiar, btnCerrar;
    private int pos=0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_cambio_moneda);

        iniciarComponentes();

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                float pesos=0.0f, total=0.0f;

                if(txtCantidad.getText().toString().matches("")){
                    Toast.makeText(getApplicationContext(),"Faltó capturar",Toast.LENGTH_SHORT).show();
                }else{
                    pesos = Float.parseFloat(txtCantidad.getText().toString());
                    switch(pos){
                        case 0: //Pesos a dolares americanos
                            total= (float) (pesos*0.06);
                            break;
                        case 1: //Pesos a dolares canadienses
                            total =(float)(pesos*0.082);
                            break;
                        case 2: //Pesos a Euros
                            total =(float)(pesos*0.055);
                            break;
                        case 3: //Pesos a libras
                            total =(float)(pesos*0.047);
                            break;
                    }
                    txtResultado.setText("Resultado: "+ total);
                }
            }
        });

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                pos=i;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtResultado.setText("Resultado: ");
                txtCantidad.setText("");
                spinner.setSelection(0);
            }
        });

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }


    public void iniciarComponentes() {
        txtCantidad = (EditText) findViewById(R.id.txtCantidad);
        txtResultado = (TextView) findViewById(R.id.txtResultado);
        spinner = (Spinner) findViewById(R.id.spnMonedas);
        btnCalcular = (Button) findViewById(R.id.btnCalcular);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnCerrar = (Button) findViewById(R.id.btnCerrar);


        String[] monedasArray = getResources().getStringArray(R.array.moneda);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, monedasArray);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(adapter);

    }

}